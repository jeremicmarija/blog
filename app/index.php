<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ob_start();
session_start();

if(isset($_SESSION['user_id'] )){
    $users = new \Models\User();
    $user= $users->getById($_SESSION['user_id']);
    $user_id = $user['id'];
}

include __DIR__ . '/views/partials/header.php';

$page = filter_input(INPUT_GET, 'page');

if (false == $page) {
    $page = 'home';
}

require_once __DIR__ . '/controllers/' . $page . '.php';

include __DIR__ . '/views/partials/footer.php';







