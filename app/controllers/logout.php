<?php
$_SESSION['isLogged'] = false;
session_destroy();

exit(header('Location: index.php?page=login'));