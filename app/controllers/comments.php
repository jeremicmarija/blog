<?php
$action = filter_input(INPUT_GET, 'action');
$comm = new \Models\Comment();

/***
 * We have action for all CRUD routes
 *
 ***/

switch($action) {

    case('addcomment'):

        if(isset($user_id)){

            $title = filter_input(INPUT_POST, 'title');

            $comment = filter_input(INPUT_POST, 'comment');

            $post_id = filter_input(INPUT_GET, 'id');

            $status = 1;

            if(!empty($_POST)){

                $error = false;
                $errorArray = [];


                if (empty($title) || strlen($title) < 3) {
                    $error = true;
                    $errorArray['title'] = 'Title shoud have min 3 characters';
                }
                if (empty($comment) || strlen($comment) < 10) {
                    $error = true;
                    $errorArray['commnet'] = 'Commnet shoud have min 10 characters';
                }
                if (!$error) {


                    $params = [
                        'title' => $title,
                        'comment' => $comment,
                        'post_id' => $post_id,
                        'user_id' => $user_id,
                        'status' => $status
                    ];

                    if ($comm->create($params)){
                        header('Location: ' . $_SERVER['HTTP_REFERER']);
                    }
                }else{

                    header("Location: index.php?page=posts&action=allposts");

                }
            }
        }
        include __DIR__ . '/../views/post.php';
        break;

    case ('editcomment') :

        //var_dump($_POST);
        $id = filter_input(INPUT_GET, 'id');

        $comment = $comm->getById($id);


        if(isset($user_id) && $user_id == $comment['user_id']){

            $title = filter_input(INPUT_POST, 'title');

            $comments = filter_input(INPUT_POST, 'comment');

            $post_id = filter_input(INPUT_GET, 'id');

            $status = 1;

            if(!empty($_POST)){

                $error = false;
                $errorArray = [];


                if (empty($title) || strlen($title) < 3) {
                    $error = true;
                    $errorArray['title'] = 'Title shoud have min 3 characters';
                }
                if (empty($comments) || strlen($comments) < 10) {
                    $error = true;
                    $errorArray['commnet'] = 'Commnet shoud have min 10 characters';
                }
                if (!$error) {

                    $params = [
                        'title' => $title,
                        'comment' => $comments,

                    ];

                    if ($comm->edit($id,$params)){
                        header('Location: ' . $_SERVER['HTTP_REFERER']);
                    }
                }else{

                    header("Location: index.php?page=posts&action=allposts");

                }
            }
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        include __DIR__ . '/../views/post.php';
        break;

    case ('deletecomment'):

        $id = filter_input(INPUT_GET, 'id');

        $comment = $comm->getById($id);

        if (isset($user_id) && $user_id == $comment['user_id']) {

            if ($comm->delete($id)) {
                header('Location: ' . $_SERVER['HTTP_REFERER']);

            }
        }else{
            header("Location: index.php?page=posts&action=allposts");
            return;
        }
        include __DIR__ . '/../views/post.php';
        break;

    default:
        return;
}