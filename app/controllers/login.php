<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/24/17
 * Time: 1:49 PM
 */

/***
 *  In this part of code first of all we must validate all date,than login User if all good...
 *
 ***/
if(!empty($_POST)){
    $errorArray = [];

    $email = filter_input(INPUT_POST, 'email');

    $password = filter_input(INPUT_POST, 'password');

    if(empty($email) || empty($password)) {
        $errorArray['email']= 'Missing mail';

        $errorArray['password']= 'Missing password';

        include __DIR__ . '/../views/login.php';

        return;
    }

    $users = new \Models\User();

    $result= $users->getUserByEmail($email);

    if (empty($result)) {
        $errorArray['email']= 'User not found';
    } else if (false === password_verify($password, $result['password'])){
        $errorArray['password']= 'Password not correct';
    }

    if (empty($errorArray)) {

        $_SESSION['user_id'] = $result['id'];
        $_SESSION['isLogged']= 'true';

        exit(header('Location: index.php?page=home'));

        return;
    }
}


include __DIR__ . '/../views/login.php';