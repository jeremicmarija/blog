<?php
$action = filter_input(INPUT_GET, 'action');
$cat = new \Models\Category();

/***
 * We have action for all CRUD routes
 *
***/
switch($action) {
    case('allcategories'):

        $categories = $cat->getAll();

        include __DIR__ . '/../views/categories.php';

        break;

    case('addcategory'):

        if(!empty($_POST)) {

            $error = false;
            $errorArray = [];

            if (isset($user_id)) {

                $name = filter_input(INPUT_POST, 'name');
                $status = 1;

                if (empty($name) || strlen($name) < 3) {
                    $error = true;
                    $errorArray['name'] = 'Category name shoud have min 3 characters';
                }
                if (!$error) {
                    $params = [
                        'name' => $name,
                        'user_id' => $user_id,
                        'status' => $status
                    ];
                    if ($cat->create($params)) {
                        header("Location: index.php?page=categories&action=allcategories");
                        return;
                    }
                }
            }
        }
        include __DIR__ . '/../views/addcategory.php';
        break;

    case ('editcategory') :

        $id = filter_input(INPUT_GET, 'id');

        $category= $cat->getById($id);

        if(!empty($_POST)) {

            $error = false;
            $errorArray = [];

            if (isset($user_id) && $user_id == $category['user_id']) {
                $name = filter_input(INPUT_POST, 'name');
                $status = 1;

                if (empty($name) || strlen($name) < 3) {
                    $error = true;
                    $errorArray['name'] = 'Category name shoud have min 3 characters';
                }
                if (!$error) {
                    $params = [
                        'name' => $name,
                        'user_id' => $user_id,
                        'status' => $status
                    ];
                    if ($cat->edit($id, $params)) {
                        header("Location: index.php?page=categories&action=allcategories");
                        return;
                    }

                }
            }
        }
        include __DIR__ . '/../views/editcategory.php';
        break;

    case ('deletecategory'):

        $id = filter_input(INPUT_GET, 'id');

        $category= $cat->getById($id);

        if (isset($user_id) && $user_id == $category['user_id']) {
            if ($cat->delete($id)) {
                header("Location: index.php?page=categories&action=allcategories");
                return;
            } else {
                echo 'fail';
                return;
            }
        }
        break;
    default:
        return;
}


