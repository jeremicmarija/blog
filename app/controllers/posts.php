<?php
$action = filter_input(INPUT_GET, 'action');

$categ = new \Models\Category();
$blogss = new \Models\Post();
$comm = new \Models\Comment();
$users = new \Models\User();

/***
 * We have action for all CRUD routes
 *
 ***/
switch($action) {
    case('allposts'):

         $blogs = $blogss->getAll();

        include __DIR__ . '/../views/posts.php';
        break;

    case('addpost'):

        if(isset($user_id)){
            $categories = $categ->getAll();

            $title = filter_input(INPUT_POST, 'title');

            $content = filter_input(INPUT_POST, 'content');

            $category_id = filter_input(INPUT_POST, 'category_id');

            $status = 1;

            if(!empty($_POST)){
                $error = false;
                $errorArray = [];


            if (empty($title) || strlen($title) < 3) {
                $error = true;
                $errorArray['title'] = 'Title shoud have min 3 characters';
            }
            if (empty($content) || strlen($content) < 100) {
                $error = true;
                $errorArray['content'] = 'Content shoud have min 100 characters';
            }
            if (!$error) {

                $params = [
                    'title' => $title,
                    'content' => $content,
                    'category_id' => $category_id,
                    'user_id' => $user_id,
                    'status' => $status
                ];

                var_dump($params);
                if ($blogss->create($params)){
                    header("Location: index.php?page=posts&action=allposts");
                    return;
                }
            }
            }
        }
        include __DIR__ . '/../views/addpost.php';
        break;

    case ('editpost') :
        $id = filter_input(INPUT_GET, 'id');

        $blog = $blogss->getById($id);

        $categories = $categ->getAll();

        if(isset($user_id) && $user_id == $blog['user_id']){

            $title = filter_input(INPUT_POST, 'title');

            $content = filter_input(INPUT_POST, 'content');

            $category_id = filter_input(INPUT_POST, 'category_id');

            $status = 1;
            if(!empty($_POST)) {
                $error = false;
                $errorArray = [];

                if (empty($title) || strlen($title) < 3) {
                    $error = true;
                    $errorArray['title'] = 'Title shoud have min 3 characters';
                }
                if (empty($content) || strlen($content) < 100) {
                    $error = true;
                    $errorArray['content'] = 'Content shoud have min 100 characters';
                }
                if (!$error) {


                    $params = [
                        'title' => $title,
                        'content' => $content,
                        'category_id' => $category_id,
                        'user_id' => $user_id,
                        'status' => $status
                    ];

                    if ($blogss->edit($id, $params)) {
                        header("Location: index.php?page=posts&action=allposts");
                        return;
                    }

                }
            }

        }else{
            header("Location: index.php?page=posts&action=allposts");
            return;
        }



        include __DIR__ . '/../views/editpost.php';
        break;

    case ('viewpost') :

        $id = filter_input(INPUT_GET, 'id');

        $blog = $blogss->getById($id);

        $comments = $comm->getCommentsByPost($id);


        include __DIR__ . '/../views/post.php';
        break;

    case ('deletepost'):
        $id = filter_input(INPUT_GET, 'id');
        $blog = $blogss->getById($id);

        if (isset($user_id) && $user_id == $blog['user_id']) {
            if ($blogss->delete($id)) {
                header("Location: index.php?page=posts&action=allposts");
                return;
            } else {
                echo 'fail';
                return;
            }
        }
        break;
    default:
        return;
}
