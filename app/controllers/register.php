<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/24/17
 * Time: 1:49 PM
 */
/***
 *  In this part of code first of all we must validate all date,than create new User if all pass...
 *
 ***/
if (!empty($_POST)) {

    $error = false;
    $errorArray = [];

    if (empty($_POST['first_name']) || strlen($_POST['first_name']) < 3) {
        $error = true;
        $errorArray['first_name'] = 'First name shoud have min 3 characters';
    }
    if (empty($_POST['last_name']) || strlen($_POST['last_name']) < 3) {
        $error = true;
        $errorArray['last_name'] = 'Last name shoud have min 4 characters';
    }

    if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $error = true;
        $errorArray['email'] = 'E-mail is required.Not valid email';
    }

    if (empty($_POST['password']) || strlen($_POST['password']) < 5) {
        $error = true;
        $errorArray['password'] = 'Password shoud have min 5 characters';
    }

    $password = $_POST['password'];

    $hashed_password = password_hash($password, PASSWORD_DEFAULT);

    $params = [
        'first_name' => $_POST['first_name'],
        'last_name' => $_POST['last_name'],
        'email' => $_POST['email'],
        'password'=> $hashed_password,
        'status' => 1

    ];

   if (!$error) {
            $user = new \Models\User();

            if ($user->create($params)) {

                $newUser = $user->getUserByEmail($_POST['email']);

                $_SESSION['isLogged']= true;

                $_SESSION['user_id'] = $newUser['id'];

                header("Location: index.php?page=home");

                return;
            }
        }
    }


include __DIR__ . '/../views/register.php';