$(document).ready(function(){

    $('.no-display').hide();

    $('.delete').on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(){
                $(location).attr('href', href);
            });

    });


    $('.edit').on('click', function (e){

       e.preventDefault();

       var divcomment = $(this).parent();
       var title = divcomment.find('.comment-title').html();
       var comment = divcomment.find('.comment-description').html();
       var fullcomment = divcomment.parent().find('.no-display');
       fullcomment.show();


       fullcomment.find('#title').val(title);
       fullcomment.find('#content').val(comment);


    });

});