<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> Blog </title>
    <link rel="stylesheet" href="../blog/app/public/css/style.css">
    <link rel="stylesheet" href="../blog/app/public/css/sweetalert.css">
    <link rel="stylesheet" href="../blog/app/public/css/font-awesome.css">
    <script type="text/javascript"  src="../blog/app/public/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../blog/app/public/js/bootstrap.min.js"></script>
    <script type="text/javascript"  src="../blog/app/public/js/custom.js"></script>
    <script type="text/javascript" src="../blog/app/public/js/sweetalert.min.js"></script>
    <link rel="stylesheet"  href="../blog/app/public/css/bootstrap.min.css">

</head>
<body>
<div class="nav">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php?page=home">Home<span class="sr-only">(current)</span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Articles <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?page=posts&action=allposts">Blogs</a></li>
                            <li><a href="index.php?page=categories&action=allcategories">Categories</a></li>
                            <li role="separator" class="divider"></li>
                            <?php if (isset($user_id)){?>
                                <li><a href="index.php?page=categories&action=addcategory">Add Category</a></li>
                                <li><a href="index.php?page=posts&action=addpost">Add Blog</a></li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if (isset($user_id)){?>
                        <li><a href="index.php?page=logout">Logout</a></li>
                    <?php }else{?>
                        <li><a href="index.php?page=register">Register</a></li>
                        <li><a href="index.php?page=login">Login</a></li>
                    <?php  } ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div><!-- end .nav -->