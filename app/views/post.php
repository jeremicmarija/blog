<div class="content">
    <div class="row">
        <div class="col-md-offset-2 col-md-6">
            <div class="post">
                <?php
                    echo '<h2>'. $blog['title'].'</h2>';
                    echo '<p>'.$blog['content'].'</p>';
                    if(isset($user_id) && $user_id == $blog['user_id']){
                        echo '<a href="index.php?page=posts&action=editpost&id='. $blog['id'].'" class="btn btn-primary">Edit</a>';
                        echo '<a href="index.php?page=posts&action=deletepost&id='. $blog['id'].'" class="btn btn-danger delete">Delete</a>';
                    }
                    echo '<hr>';
               ?>
            </div>
        </div>
    </div>
</div>
<?php
foreach($comments as $comment){
    $user = $users->getById($comment['user_id']);
    ?>
<div class="commentfull">
    <div class="col-md-offset-2 col-md-6 comments">
         <?php
        echo '<p class="comment-user">' . $user['first_name'] . ' ' . $user['last_name'] .'</p>';
        echo '<p class="comment-title">' . $comment['title'] . '</p>';
        echo '<p class="comment-description">' . $comment['comment'] . '</p>';
        if(isset($user_id) && ($comment['user_id'] == $user_id)){
            echo'<a class="edit"  href="index.php?page=comments&action=editcomment&id='. $comment['id'].'"><i class="fa fa-pencil icon" aria-hidden="true"></i></a>';
            echo '<a class="delete" href="index.php?page=comments&action=deletecomment&id='. $comment['id'].'"><i class="fa fa-trash-o icon" aria-hidden="true"></i></i></a>';
        }
        ?>
    </div>
    <div class="row no-display">
        <div class="col-md-offset-3 col-md-6">
            <form id="send"  action="index.php?page=comments&action=editcomment&id=<?php echo $comment['id']?>"  method="post">
                <div class="col-md-4 form-group">
                    <input type="text" name="title" class="form-control" id="title" placeholder="Comment Title">
                    <?php if (!empty($errorArray['title'])) { ?>
                        <span id="helpBlock2" class="help-block error"><?php echo $errorArray['title']; ?></span>
                    <?php } ?>
                </div>
                <div class="col-md-6 form-group">
                    <textarea rows="4" name="comment" class="form-control" id="content" placeholder="Content"></textarea>
                    <?php if (!empty($errorArray['comment'])) { ?>
                        <span id="helpBlock2" class="help-block error"><?php echo $errorArray['comment']; ?></span>
                    <?php } ?>
                </div>
                <button type="submit" class="btn btn-success">Edit</button>
            </form>
        </div>
    </div>
</div>
    <?php
    }
    ?>

<?php

if(isset($user_id)) {
    ?>
    <div class="row">
        <div class="col-md-offset-2 col-md-6">
            <form id="send"  action="index.php?page=comments&action=addcomment&id=<?php echo $blog['id']?>"  method="post">
                <div class="col-md-4 form-group">
                    <input type="text" name="title" class="form-control" id="title" placeholder="Comment Title">
                    <?php if (!empty($errorArray['title'])) { ?>
                        <span id="helpBlock2" class="help-block error"><?php echo $errorArray['title']; ?></span>
                    <?php } ?>
                </div>
                <div class="col-md-6 form-group">

                    <textarea rows="4" name="comment" class="form-control" id="content" placeholder="Content"></textarea>
                    <?php if (!empty($errorArray['comment'])) { ?>
                        <span id="helpBlock2" class="help-block error"><?php echo $errorArray['comment']; ?></span>
                    <?php } ?>
                </div>
                <button type="submit" class="btn btn-success">Submit</button>
            </form>
        </div>
    </div>
    <?php
}
