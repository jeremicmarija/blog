<div class="row">
    <div class=" col-md-offset-3 col-md-6">
        <div class="alert "></div>
        <h1>Edit Post :</h1><hr>

        <form id="send" action="index.php?page=posts&action=editpost&id=<?php echo $blog['id']?>" method="post">
            <div class="form-group">
                <label for="category">Select category:</label>
                <select name="category_id">
                    <?php foreach($categories as $category){?>
                        <option value="<? echo $category['id']?>"><?php echo $category['name']?></option>
                    <?php }?>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" id="title" placeholder="Title" value="<? echo $blog['title']?>">
                <?php if (!empty($errorArray['title'])) { ?>
                    <span id="helpBlock2" class="help-block"><?php echo $errorArray['title']; ?></span>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea rows="10" name="content" class="form-control" id="content" placeholder="Content" ><?php echo $blog['content']?></textarea>
                <?php if (!empty($errorArray['content'])) { ?>
                    <span id="helpBlock2" class="help-block"><?php echo $errorArray['content']; ?></span>
                <?php } ?>
            </div>

            <button type="submit"  class="btn btn-success">Submit</button>
        </form>
    </div>
</div>