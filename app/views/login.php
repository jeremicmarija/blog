<div class="row">
    <div class=" col-md-offset-3 col-md-6">
        <h1>Login :</h1><hr>
        <form action="index.php?page=login" method="post">
            <div class="form-group">
                <label name="email">Email:</label>
                <input type="email" id="email" name="email" class="form-control">
                <?php if (!empty($errorArray['email'])) { ?>
                    <span id="helpBlock2" class="help-block error"><?php echo $errorArray['email']; ?></span>
                <?php } ?>
            </div>
            <div class="form-group">
                <label name="subject">Password:</label>
                <input type="password" id="password" name="password" class="form-control">
                <?php if (!empty($errorArray['password'])) { ?>
                    <span id="helpBlock2" class="help-block error"><?php echo $errorArray['password']; ?></span>
                <?php } ?>
            </div>
            <input type="submit" value="Send" class="btn btn-success">
        </form>
    </div>
</div>