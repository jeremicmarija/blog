<div class="row">
    <div class=" col-md-offset-3 col-md-6">
        <h1>Register:</h1><hr>
        <form action="index.php?page=register" method="post">
            <div class="form-group">
                <label name="first_name">Firts Name:</label>
                <input type="first_name" id="first_name" name="first_name" class="form-control">
                <?php if (!empty($errorArray['first_name'])) { ?>
                    <span id="helpBlock2" class="help-block error"><?php echo $errorArray['first_name']; ?></span>
                <?php } ?>
            </div>
            <div class="form-group">
                <label name="last_name">Last Name:</label>
                <input type="last_name" id="last_name" name="last_name" class="form-control">
                <?php if (!empty($errorArray['last_name'])) { ?>
                    <span id="helpBlock2" class="help-block error"><?php echo $errorArray['last_name']; ?></span>
                <?php } ?>
            </div>
            <div class="form-group">
                <label name="email">Email:</label>
                <input type="email" id="email" name="email" class="form-control">
                <?php if (!empty($errorArray['email'])) { ?>
                    <span id="helpBlock2" class="help-block error"><?php echo $errorArray['email']; ?></span>
                <?php } ?>
            </div>
            <div class="form-group">
                <label name="subject">Password:</label>
                <input type="password" id="password" name="password" class="form-control">
                <?php if (!empty($errorArray['password'])) { ?>
                    <span id="helpBlock2" class="help-block error"><?php echo $errorArray['password']; ?></span>
                <?php } ?>
            </div>
            <input type="submit" value="Send" class="btn btn-success">
        </form>
    </div>
</div>