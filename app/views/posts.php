<div class="content">
    <div class="row">
        <div class="col-md-offset-2 col-md-6">
            <div class="post">
                <?php foreach($blogs as $blog) {
                    echo '<h2>'. $blog['title'].'</h2>';
                    echo '<p>'.substr($blog['content'], 0, 700).'<strong>...</strong></p>';
                    echo '<a class="btn btn-primary" href="index.php?page=posts&action=viewpost&id='. $blog['id'].'" >Read more</a>';
                    echo '<hr>';
                }?>
            </div>
        </div>
    </div>
</div>