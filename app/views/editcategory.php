<div class="row">
    <div class=" col-md-offset-3 col-md-6">
        <div class="alert "></div>
        <h1>Add new post :</h1><hr>

        <form id="send" action="index.php?page=categories&action=editcategory&id=<?php echo $category['id']?>" method="post">
            <div class="form-group">
                <label for="title">Category name:</label>
                <input type="text" name="name" class="form-control"  placeholder="Category name..." value="<?php echo $category['name']?>">
                <?php if (!empty($errorArray['name'])) { ?>
                    <span id="helpBlock2" class="help-block"><?php echo $errorArray['name']; ?></span>
                <?php } ?>
            </div>
            <button type="submit"  class="btn btn-success">Submit</button>
        </form>
    </div>
</div>