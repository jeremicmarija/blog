<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <div class="home_title">
                    <?php
                    if(isset($user_id)){
                        ?>
                        <h2>Welcome to our site!</h2>
                        <p>Hello dear <?php echo $user['first_name'] ?>! You can add new blog,category,write comment,edit and delete.</p>
                        <?php
                    }else{
                        ?>
                        <h2>Welcome to our site!</h2>
                        <p>Hello dear user! You can read posts and comments.If you want add new blog,category or add comment please login.</p>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div><!-- end .jumbotron -->
    <div class="content">
        <div class="row">
            <div class="col-md-offset-2 col-md-6">
                <div class="post">
                    <?php foreach($blogs as $blog) {
                        echo '<h2>'. $blog['title'].'</h2>';
                        echo '<p>'.substr($blog['content'], 0, 700).'<strong>...</strong></p>';
                        echo '<a class="btn btn-primary" href="index.php?page=posts&action=viewpost&id='. $blog['id'].'" >Read more</a>';
                        echo '<hr>';
                    }?>
                </div>
            </div>
            </div>
        </div>
    </div><!-- end .content-->

