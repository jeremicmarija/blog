<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/23/17
 * Time: 6:31 PM
 */

namespace Interfaces;


interface Crud
{
    public function create(array $params);

    public function getAll();

    public function getById($id);

    public function edit($id, array $params);

    public function delete($id);

}