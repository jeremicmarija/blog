-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: blog
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (18,'Programming',8,1),(19,'Music',8,1),(20,'Sport',8,1);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `post_id_idx` (`post_id`),
  CONSTRAINT `id_post` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `id_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (7,'I love PHP ','I love PHP because it\'s simple and great!',14,8,1),(8,'I want rock!!!!!!','Rock music is so aaaaaaaaaaaa.....crazzy....good....!!!!!!',15,8,1);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user_idx` (`user_id`),
  KEY `user_idx` (`user_id`),
  KEY `category_idx` (`category_id`),
  CONSTRAINT `category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (14,'PHP Course','Proin tincidunt vitae odio vel auctor. Ut cursus eleifend nibh, ac feugiat dui venenatis quis. Nullam purus ante, fringilla eu cursus id, vulputate id est. Suspendisse nulla ex, auctor ac risus at, pretium finibus sem. Ut sodales ex at justo consequat interdum. Phasellus erat justo, consectetur lacinia neque non, consequat lacinia felis. Ut convallis ipsum ut urna imperdiet, nec fermentum risus pellentesque. Integer pharetra dapibus ex, sit amet dictum augue gravida tempus. Aliquam erat volutpat. Nulla tincidunt accumsan velit, vel euismod sapien suscipit at. Cras porta placerat sem.\r\n\r\nInteger erat risus, blandit in risus et, imperdiet congue sapien. Curabitur non eros sodales metus euismod pretium vitae sed neque. Etiam ac metus nulla. Aenean eget rhoncus sapien, sit amet dictum dui. Donec mollis cursus urna ac venenatis. Nullam ornare nibh eget vestibulum placerat. Etiam cursus laoreet eros eget auctor. Sed sit amet leo est. Etiam sit amet urna purus.\r\n\r\nUt a condimentum dui. Nam tempus hendrerit pulvinar. Suspendisse potenti. Aenean ut cursus enim, vitae feugiat est. Pellentesque sed ultricies turpis, non viverra diam. Sed varius, urna quis finibus facilisis, libero nunc feugiat felis, in dapibus elit nisl ac nisl. Etiam quis egestas turpis.\r\n\r\nDonec at nisi et dolor lacinia volutpat. Sed blandit consectetur eros, eget commodo magna lacinia at. Maecenas blandit egestas maximus. Ut euismod, ex eu volutpat lobortis, felis risus consequat dolor, non tincidunt augue massa vitae nisl. Donec vitae maximus leo, sed varius lorem. Suspendisse diam enim, molestie ut pharetra facilisis, placerat quis quam. Proin tincidunt ex sem, ac auctor erat aliquet in.',18,8,1),(15,'Rock Music','Maecenas mollis velit a dui vehicula, molestie semper elit eleifend. Aliquam erat volutpat. Ut et magna eget nibh cursus facilisis eu iaculis urna. Morbi at diam id mi sollicitudin hendrerit in ut dolor. Quisque facilisis vitae mauris in fermentum. Nulla ullamcorper enim tristique sapien auctor, eu eleifend nisi tincidunt. Phasellus finibus lacinia sollicitudin. Donec molestie tellus ac bibendum vehicula. Morbi sollicitudin faucibus nisi, ac pellentesque justo condimentum a.\r\n\r\nCras sit amet ipsum vel neque auctor sagittis in molestie sapien. Pellentesque erat erat, convallis ut metus vitae, euismod placerat nisl. Etiam porta elementum massa. Donec eget eleifend dui. Etiam non ligula hendrerit, luctus risus sed, molestie massa. Nulla placerat urna orci, sed iaculis risus iaculis vitae. Maecenas viverra lacinia iaculis. Aenean interdum lorem orci, in facilisis nulla vehicula sed. Duis tempor lacinia laoreet. Nulla non massa tincidunt, tempus nunc in, gravida tortor.\r\n\r\nIn hac habitasse platea dictumst. Vivamus consequat luctus fringilla. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pretium lorem nec lacus semper maximus. Cras in elementum erat, ut ultricies justo. Vestibulum vestibulum ipsum nec orci maximus, porta luctus tortor aliquet. Mauris facilisis nisl vitae tortor euismod finibus. Suspendisse potenti. Curabitur id odio eget mauris fermentum elementum. Ut vehicula elit vel lacus ultricies scelerisque. Nullam leo tortor, dictum a urna nec, sodales convallis mi. Aenean vitae tortor leo. Suspendisse a orci id ex facilisis pharetra sit amet sed tortor. Aliquam fringilla diam ultrices, pellentesque dui ut, dapibus tortor.',19,8,1);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (8,'Marija','Jeremic','jeremic.marija1106@gmail.com','$2y$10$K.rBQun6Gx5I7O0iq0rzWOxv/7G3JgS0bZCEk25E9qV34yhWEfoaS',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-26  1:53:10
