<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/23/17
 * Time: 3:06 PM
 */
namespace Models;



class User extends Core
{

   protected $tableName = 'users';

   protected $primaryKey = 'id';


    public function getUserByEmail($email)
    {
        $query = $this->db->prepare('SELECT * FROM users WHERE email=?');

        if($query && $query->execute([$email])){
            return $query->fetch(\PDO::FETCH_ASSOC);
        }
        return null;
    }
}