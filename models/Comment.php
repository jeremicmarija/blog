<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/23/17
 * Time: 3:58 PM
 */

namespace Models;

class Comment extends Core
{
    protected $tableName = 'comments';

    public function getCommentsByPost($post_id)
    {
        $query =$this->db->prepare("SELECT * FROM `{$this->tableName}` WHERE post_id=? AND status=1 ORDER by id DESC") ;

        if($query && $query->execute([$post_id])){
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        }
        return null;
    }



}