<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/23/17
 * Time: 3:56 PM
 */

namespace Models;
use PDO;


class Category extends Core
{
    protected $tableName = 'categories';

    protected $primaryKey = 'id';

}