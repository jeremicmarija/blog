<?php

namespace Models;
use Interfaces\Crud;

/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/23/17
 * Time: 3:11 PM
 */

abstract class Core implements Crud
{
    protected $db;

    protected $tableName;

    protected $primaryKey = 'id';

    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    public function create(array $params)
    { //var_dump($params);
        if (empty($this->tableName) || empty($this->primaryKey)) {
            throw new \Exception('Missing table name or primary key');
        }

        $query = "INSERT INTO `{$this->tableName}` (`" . implode('`, `', array_keys($params)) . "`) VALUES";


        $placeholders = [];

        for ($count = 0; $count < count($params); $count++) {
            $placeholders[] = '?';
        }

        $query .= '(' . implode(', ', $placeholders) . ')';


        $statement = $this->db->prepare($query);

        if (false === $statement) {
            throw new \PDOException('Cannot prepare query.');
        }

        if (false === $statement->execute(array_values($params))) {
            throw new \PDOException('Invalid query. ' . $query);
        }

        return $statement->rowCount();
    }

    public function edit($id,array $params)
    {
        if (empty($this->tableName) || empty($this->primaryKey)) {
            throw new \Exception('Missing table name or primary key');
        }

        $query = "UPDATE `{$this->tableName}` SET ";
        $criteria = [];

        foreach ($params as $field => $value) {
            $criteria[] = "`{$field}` = ?";
        }
        $query .= implode(', ', $criteria) ." WHERE `{$this->primaryKey}` = ?" ;



        $statement = $this->db->prepare($query);
        if (false === $statement) {
            throw new \PDOException('Cannot prepare query.');
        }

        $data = array_values($params);

        $data[] = $id;

        if (false === $statement->execute($data)) {
            throw new \PDOException('Invalid query. ' . $query);
        }

        return $statement->rowCount();
    }

    public function getAll()
    {
        if(empty($this->tableName)){
          throw new \Exception('Missing table name.');
        }

        $query =$this->db->prepare("SELECT * FROM `{$this->tableName}` WHERE status=1") ;

        if($query && $query->execute([])){
            return $query->fetchAll(\PDO::FETCH_ASSOC);
        }
        return null;
    }

    public function getById($id)
    {
        if (empty($this->tableName) || empty($this->primaryKey)) {
            throw new \Exception('Missing table name or primary key');
        }

        $query = "SELECT * FROM `{$this->tableName}` WHERE `{$this->primaryKey}` = ? LIMIT 1;";

        $statement = $this->db->prepare($query);

        if (false === $statement) {
            throw new \PDOException('Cannot prepare query.');
        }

        if (false === $statement->execute([$id])) {
            throw new \PDOException('Invalid query. ' . $query);
        }

        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function delete($id)
    {
        if (empty($this->tableName) || empty($this->primaryKey)) {
            throw new \Exception('Missing table name or primary key');
        }

        $statement = $this->db->prepare("DELETE FROM `{$this->tableName}` WHERE `{$this->primaryKey}` = ?");

        $statement->execute([$id]);

        return (bool) $statement->rowCount();
    }


}
