<?php
/**
 * Created by PhpStorm.
 * User: smrda
 * Date: 9/23/17
 * Time: 3:08 PM
 */
namespace Models;


class Database
{
    private static $_instance = null;

    private function __construct(){}

    public static function getInstance(){

        if(is_null(self::$_instance)){

            self::$_instance = new \PDO("mysql:host=127.0.0.1;dbname=blog", "root", "");
        }

        return self::$_instance;

    }

}